<?php

namespace App\Http\Livewire;

use App\Services\PhotoSessionService;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\App;
use Livewire\Component;
use Livewire\TemporaryUploadedFile;
use Livewire\WithFileUploads;

class PhotoSessionForm extends Component
{
    use WithFileUploads;

    const MAX_FILE_SIZE_KB = 40960;

    public string $name = "";
    /**
     * @var array<TemporaryUploadedFile>
     */
    public $photos = [];

    private PhotoSessionService $photoSessionService;

    public function mount() {
        $this->name = "Test";
        $this->photoSessionService = App::make(PhotoSessionService::class);
    }

    public function updatedPhotos()
    {
        $this->validate([
            'photos.*' => "image|max:".self::MAX_FILE_SIZE_KB
        ]);
    }


    public function save()
    {
        $this->validate([
            'photos.*' => 'image|max:'.self::MAX_FILE_SIZE_KB, // 1MB Max
        ]);

        foreach ($this->photos as $photo) {
            $photo->store('photos');
        }

        $this->photoSessionService->store([ 'name' => $this->name, 'photos' => $this->photos ]);
    }

    public function render()
    {
        return view('livewire.photo-session-form');
    }
}
