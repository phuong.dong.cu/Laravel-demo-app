<?php

namespace App\Http\Controllers;

use App\Models\PhotoSessions;
use App\Services\PhotoSessionService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;
use Illuminate\View\View;
use Ramsey\Uuid\Type\Integer;

class PhotoSessionController extends Controller
{
    private PhotoSessionService $photoSessionService;

    public function __construct(PhotoSessionService $photoSessionService)
    {
        $this->photoSessionService = $photoSessionService;
    }

    public function create()
    {
        return view('photo-sessions/create');
    }

    public function store(Request $request)
    {
        $data = $request->all();

        $data = $this->validate($request, [
            'name' => 'string',
            'photos.*' => ['image', 'max:1024']
        ]);
        foreach ($data['photos'] as $photo) {
            error_log(print_r($photo, true));
        }
        return view('welcome');
    }

    public function index(Request $request): View
    {
        $user_id = $request->query('user_id');
//        $photos = PhotoSessions::all()->where('user_id', $id)->where('published', true);
        $photos = DB::table('photo_sessions')
            ->where('user_id', $user_id)
            ->where('published', true)
            ->get();

        return view('photo-sessions/photo', ['photos' => $photos]);
    }
}
