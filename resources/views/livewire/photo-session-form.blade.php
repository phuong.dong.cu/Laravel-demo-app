<div>
    <form id="form" method="post" action="{{route('photo-sessions.store')}}" enctype="multipart/form-data">
        @csrf
        @foreach($photos as $photo)
            Photo Preview:
            <img src="{{ $photo->temporaryUrl() }}" style="width: 128px">
        @endforeach

        <div>
            <label>
                Name
                <input name="name" wire:model="name" />
            </label>
        </div>
        <div id="dropContainer" style="border:1px solid black;height:100px;">
            Drop Here
        </div>
        <input type="file" id="fileInput" name="photos[]" wire:model="photos" multiple/>
        @error('photos.*') <span class="error">{{ $message }}</span> @enderror
        <button type="submit">Submit</button>
    </form>

    <script>
        const dropContainer = document.getElementById("dropContainer");
        const fileInput = document.getElementById("fileInput");

        dropContainer.ondragover = dropContainer.ondragenter = function(evt) {
            evt.preventDefault();
        };

        dropContainer.ondrop = function(evt) {
            const prevFiles = fileInput.files;
            const dT = new DataTransfer();

            for(let i = 0; i < prevFiles.length; ++i) {
                dT.items.add(prevFiles[i]);
            }

            for(let i = 0; i < evt.dataTransfer.files.length; ++i) {
                dT.items.add(evt.dataTransfer.files[i]);
            }

            fileInput.files = dT.files;
        @this.uploadMultiple('photos', dT.files);
            evt.preventDefault();
        };

        window.addEventListener("dragover",function(e){
            e = e || event;
            e.preventDefault();
        },false);
        window.addEventListener("drop",function(e){
            e = e || event;
            e.preventDefault();
        },false);
    </script>
</div>
